import shelve

d = shelve.open('books')

# d['test'] = 123

# d['i1'] = {
#     'tag': ['red'],
#     'name': 'item1',
#     'content': 'lalaal1'
# }
# d['i2'] = {
#     'tag': ['red'],
#     'name': 'item2',
#     'content': 'lalaal2'
# }
# d['i3'] = {
#     'tag': ['red', 'blue'],
#     'name': 'item3',
#     'content': 'lalaal3'
# }
# d['i4'] = {
#     'tag': ['yellow', 'blue'],
#     'name': 'item4',
#     'content': 'lalaal4'
# }

# print('test' in d)
for x in list(d.keys()):
    if 'red' in d[x]['tag']:
        print(x)

d.close()