#!/usr/bin/env python
"""
The good Sebastian prepares everything for me!

Usage:
    Sebastian (-i | --interactive)
    Sebastian (-h | --help | --version)

Options:
    -i, --interactive  Interactive Mode
    -h, --help  Show this screen and exit.
"""

import sys
import cmd
from docopt import docopt, DocoptExit


def docopt_cmd(func):
    """
    This decorator is used to simplify the try/except block and pass the result
    of the docopt parsing to the called action.
    """
    def fn(self, arg):
        try:
            opt = docopt(fn.__doc__, arg)

        except DocoptExit as e:
            # The DocoptExit is thrown when the args do not match.
            # We print a message to the user and the usage block.

            print('Invalid Command!')
            print(e)
            return

        except SystemExit:
            # The SystemExit exception prints the usage for --help
            # We do not need to do the print here.

            return

        return func(self, opt)

    fn.__name__ = func.__name__
    fn.__doc__ = func.__doc__
    fn.__dict__.update(func.__dict__)
    return fn


class MyInteractive(cmd.Cmd):
    intro = '🍀 How can I serve, my lord?' \
        + ' (Type "help" for a list of commands.)'
    prompt = '=> '
    Logarex = {
        "customer": "LOGAREX SMART METERING S.R.O",
        "contact": "郑楚晓"
    }
    current_order = None

    @docopt_cmd
    def do_ls(self, arg):
        """ls -- list items

Usage: ls [TAG ...]

Options:

        """
        print(arg)

    @docopt_cmd
    def do_tag(self, arg):
        """tag -- tag item

Usage: tag TAG ... [--item=ITEM]

Options:
    --item=ITEM  Item [default: CURRENT_ITEM]
        """
        print(arg)

    @docopt_cmd
    def do_rmtag(self, arg):
        """rmtag -- remove tag from item

Usage: rmtag TAG ... [--item=ITEM]

Options:
    --item=ITEM  Item [default: CURRENT_ITEM]
        """
        print(arg)

    @docopt_cmd
    def do_new(self, arg):
        """new -- create new item

Usage: new [ITEM ...]

Options:
        """
        print(arg)

    @docopt_cmd
    def do_cp(self, arg):
        """cp -- copy items

Usage: cp <source_item> <target_item>

Options:

        """
        print(arg)

    @docopt_cmd
    def do_update(self, arg):
        """update -- update item contents

Usage: update <content> <value> [--item=ITEM]

Options:
    --item=ITEM  Item [default: CURRENT_ITEM]
        """
        print(arg)

    @docopt_cmd
    def do_check(self, arg):
        """check -- check item contents

Usage: check [-n] <item>

Options:
    -n  check without changing current item
        """
        print(arg)

    @docopt_cmd
    def do_make(self, arg):
        """make -- make order excel file

Usage: make [-a ITEM ...]

Options:
    -a  make order with all items in it
        """
        print(arg)

    @docopt_cmd
    def do_save(self, arg):
        """save -- save item to database

Usage: save [-c] [ITEM ...]

Options:
    -c  check the item
        """
        print(arg)

    def do_quit(self, arg):
        """Quits out of Interactive Mode."""

        print('Farewell, my lord. 🍀')
        exit()


opt = docopt(__doc__, sys.argv[1:])


if opt['--interactive']:
    MyInteractive().cmdloop()


print(opt)